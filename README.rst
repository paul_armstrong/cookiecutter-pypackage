======================
Cookiecutter PyPackage
======================

.. image:: https://gitlab.com/gitlab-data/cookiecutter-pypackage/badges/main/pipeline.svg
        :target: https://gitlab.com/gitlab-data/cookiecutter-pypackage/-/commits/main
        :alt: Documentation

Cookiecutter_ template for a Python package, specifically for repos in GitLab.
Handles pypi deployment using Docker GitLab CI pipelines.

* gitlab repo: https://gitlab.com/gitlab-data/cookiecutter-pypackage
* Documentation: https://gitlab-data.gitlab.io/cookiecutter-pypackage/
* Free software: BSD license

Features
--------

* Testing setup with ``unittest`` and ``python setup.py test`` or ``pytest``
* Tox_ testing: Setup to easily test for Python 3.6, 3.7, 3.8
* Sphinx_ docs: Documentation ready for generation with, for example, GitLab pages (default)
* bump2version_: Pre-configured version bumping with a single command
* Auto-release to PyPI_ when you push or create a new tag in GitLab (optional)
* Command line interface using Click (optional)
* Automatic GitLab CI pipelines for:
    * Static Application Security Testing (SAST)
    * Testing with pytest
    * Code linting with Black and Flake8
    * Code coverage reports
    * MyPy validation
    * Complexity check with Xenon
    * Code cleanup using Vulture
    * Documentation deployment using GitLab pages

.. _Cookiecutter: https://gitlab.com/cookiecutter/cookiecutter

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

    pip install -U cookiecutter

Generate a Python package project using SSH::

    cookiecutter git@gitlab.com:gitlab-data/cookiecutter-pypackage.git


OR using HTTPS::

    cookiecutter https://gitlab.com/gitlab-data/cookiecutter-pypackage.git

Then:

* Create a repo and put it there.
* Install the dev requirements into a virtualenv. (``pip install -r requirements_dev.txt``)
* Add a ``requirements.txt`` file that specifies the packages you will need for
  your project and their versions. For more info see the `pip docs for requirements files`_.
* Add your pypi username as environment variables to the GitLab repo as TWINE_USERNAME and TWINE_PASSWORD
* Release your package by pushing a new tag to master.


.. _`pip docs for requirements files`: https://pip.pypa.io/en/stable/user_guide/#requirements-files
.. _Register: https://packaging.python.org/tutorials/packaging-projects/#uploading-the-distribution-archives

For more details, see the `cookiecutter-pypackage tutorial`_.

.. _`cookiecutter-pypackage tutorial`: https://cookiecutter-pypackage.readthedocs.io/en/latest/tutorial.html


Fork This / Create Your Own
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have differences in your preferred setup, I encourage you to fork this
to create your own version. Or create your own; it doesn't strictly have to
be a fork.

* Once you have your own version working, add it to the Similar Cookiecutter
  Templates list above with a brief description.

* It's up to you whether or not to rename your fork/own version. Do whatever
  you think sounds good.

.. _Tox: http://testrun.org/tox/
.. _Sphinx: http://sphinx-doc.org/
.. _`pyup.io`: https://pyup.io/
.. _bump2version: https://gitlab.com/c4urself/bump2version
.. _Punch: https://gitlab.com/lgiordani/punch
.. _Poetry: https://python-poetry.org/
.. _PyPi: https://pypi.python.org/pypi
.. _Mkdocs: https://pypi.org/project/mkdocs/


Forked from https://github.com/audreyfeldroy/cookiecutter-pypackage